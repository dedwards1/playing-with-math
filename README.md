# Playing with the Mandelbrot Set

This project contains a web page that draws
visualizations of the Mandelbrot Set
when you open it in a browser like Firefox or Chrome.

To run it, save the `index.html` file onto your computer,
and then open that file in your web browser.
After a few seconds (it takes a while to draw),
the Mandelbrot Set will appear.

The `index.html` file is designed to be played with!
Open it up using a text editor
(like Notepad, or [Sublime](https://www.sublimetext.com/))
to see and modify the code it uses to draw the visualization.

The comment blocks in the `index.html` file include
instructions for how you can modify the code
to view other visualizations besides the default one
(there are a total of 3 visualizations you can try).
You could even try your hand at inventing new
visualizations yourself!

While you play around with the code, you'll want to
have the javascript console open so you can find out
about any errors. To do this, after opening the web page,
right click anywhere on the page and choose Inspect.
This will open the developer tools panel.
In the new panel, find the tab labeled "Console",
and click that.

Remember: when mathematicians do math,
it's almost never about calculating
the single correct answer to an equation.
It's about asking questions, and seeing patterns,
and wondering if there are _reasons_
why things look the way they do.
Melinda Green made a _beautiful_ discovery by
thinking about the Mandelbrot Set in a different way.
As you think about it, are there any questions you have?
Curiosities you notice?
Any places where you wonder if there might be
an interesting pattern?

Now you're doing math!
